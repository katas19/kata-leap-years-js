
class Year {
  private yearValue: number;

  constructor(value: number) {
    this.yearValue = value;
  }

  public isLeapYear(): boolean {
    if( this.isDivisibleBy100() && this.isNotDivisibleBy400() ) {
      return false
    }
    return true;
  }

  private isDivisibleBy100(): boolean {
    return this.yearValue % 100 === 0;
  }

  private isNotDivisibleBy400(): boolean {
    return this.yearValue  % 400 !== 0;
  }
}

describe('Year', () => {
  test('All years divisible by 400 ARE leap years', () => {
    expect(new Year(2000).isLeapYear()).toBeTruthy();
  });


  test.each([700, 1800, 1900, 2100])
  ('year %p divisible by 100 but not by 400 is not leap years', (value: number) => {
    expect(new Year(value).isLeapYear()).toBeFalsy();
  });
});


